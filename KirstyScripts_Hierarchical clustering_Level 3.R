# Running global habitat classification on GEBCO 2020 env vars, Bio-ORACLE data and MEDUSA model data (2006-2015) at 1/12 degree resolution (approx 10km2)
# Variables: FBPI, BBPI, Slope (GEBCO); POC flux (Yool/Medusa); nutrients etc (Bio-ORACLE)
# Hierarchical clustering
# Original code from Tiago Gandra, expanded to hierarchical clustering by KM

### LEVEL 3 CLUSTERING ON LEVEL 2 OUTPUTS (i.e. third level of clustering to give subclasses of the subclasses of highest level habitat classes)

# This code needs to be repeated for each of the higher level classes (i.e. this code is just for re-clustering of all Class 1 subclasses)

######### Load packages---------
library(raster)
library(factoextra)
library(RStoolbox)
library(reshape2)
library(corrplot)
library(fpc)
library(RColorBrewer)
library(dplyr)
library(rpart)
library(rpart.plot)
library(dplyr)
library(NbClust)

######### Import & prep rasters---------

setwd("C:/Users/kmcquaid/OneDrive - University of Plymouth/2 Methods/Data & Projects/Global data layers/Env data")

fbpi <- raster("./Topography/GEBCO2020_FBPI_10km_WGSfromMOL.tif")
bbpi <- raster("./Topography/GEBCO2020_BBPI_10km_WGSfromMOL.tif")
slope <- raster("./Topography/GEBCO2020_Slope_10km_WGSfromMOL.tif")
poc <- raster("./MEDUSA/2006-2015/2006-2015_fluxXYZ112deg.tif")
currvel <- raster("./Hydrography/Current velocity/Present.Benthic.Mean.Depth.Current.Velocity.Mean.tif.BOv2_1.tif")
dissox <- raster("./Hydrography/Nutrients/Present.Benthic.Mean.Depth.Dissolved.oxygen.Mean.tif")
nit <- raster("./Hydrography/Nutrients/Present.Benthic.Mean.Depth.Nitrate.Mean.tif")
phos <- raster("./Hydrography/Nutrients/Present.Benthic.Mean.Depth.Phosphate.Mean.tif")
sal <- raster("./WMS/Bio-ORACLE/BioO_sal_10km_WGS84.tif")
sil <- raster("./Hydrography/Nutrients/Present.Benthic.Mean.Depth.Silicate.Mean.tif")
tmp <- raster("./WMS/Bio-ORACLE/BioO_temp_10km_WGS84.tif")
depth <- raster("./Bathymetry/GEBCO2020_depth_10km_WGSfromMOL.tif")

setwd("C:/Users/kmcquaid/OneDrive - University of Plymouth/2 Methods/Data & Projects/S Atlantic/Classification/Draft 3/outputs/scrapbook")

zone1 <- raster("./Test1/Level2_zone1_4classes.tif") # output of level 2 clustering on Habitat Class 1

### Stack env rasters

# check dim of all rasters using dim()
# make raster stack of all Boi-ORACLE vars and MEDUSA poc data (as all have same extent)
vars.stack <- stack(poc, currvel, dissox, nit, phos, sal, sil, tmp)

# make raster stack of GEBCO-derived vars (as all have same extent), and crop to extent of other layers layer
geb.vars <- stack(fbpi, bbpi, slope)
geb.crop <- crop(geb.vars, vars.stack)

# make stack of all layers (now all have same extent)
var <- stack(vars.stack, geb.crop)

names(var) # to see what the layers are currently called
names(var)<-c('poc','currvel','dissox','nitrate','phosphate','salinity','silicate','temp','fbpi', 'bbpi', 'slope') # to change the names 

### Crop env vars to extent of each zone in Level 2 clustering

var.crop <- crop(var, zone1) # crop env layers to extent of clara output
names(zone1)<-'zone' # rename clustering output to something sensible
zones <- zone1

zone1_1 <- zones == 1 # select only zone 1 into new raster
zone1_1[zone1_1 < 1] <- NA # remove NAs
zone1_1var <- mask(var.crop, zone1_1) # crop raster stack of env vars to extent of zone 1
plot(zone1_1) # check it's worked
plot(zone1_1var$temp)

zone1_2 <- zones == 2
zone1_2[zone1_2 < 1] <- NA # this new raster has values 0-1 (i.e. binomial: zone 2 yes or no), so select only those values = 1
zone1_2var <- mask(var.crop, zone1_2)

zone1_3 <- zones == 3
zone1_3[zone1_3 < 1] <- NA 
zone1_3var <- mask(var.crop, zone1_3)

zone1_4 <- zones == 4
zone1_4[zone1_4 < 1] <- NA 
zone1_4var <- mask(var.crop, zone1_4)


##########      RE-CLUSTERING OF HABITAT CLASS 1 SUBCLASS 1    #############################

######## PRINCIPLE COMPONENT ANALYSIS

## PCA using "prcomp"
gc()
zone1_1var.df <- as.data.frame(zone1_1var)
zone1_1var.df <- zone1_1var.df[complete.cases(zone1_1var.df),]
head(zone1_1var.df)
res.pca<-prcomp(zone1_1var.df,scale = TRUE)
summary(res.pca)
plot(res.pca)
# PCA Wheel (loading plot):
# plotting the first two PCs (called dimensions in the plot)
#fviz_pca_var(res.pca, axes = c(1,2),
#             col.var = "contrib", # Color by contributions to the PC
#             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
#             repel = TRUE,     # Avoid text overlapping
#             title='')
#ggsave("figures/pca_wheel_1-2.jpg", dpi=150, units='in', width=7, height=7)

# plotting the second two PCs
#fviz_pca_var(res.pca, axes = c(3,4),
#             col.var = "contrib", # Color by contributions to the PC
#             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
#             repel = TRUE,     # Avoid text overlapping
#             title='')
#ggsave("figures/pca_wheel_3-4.jpg", dpi=150, units='in', width=7, height=7)

## PCA using "raster"
pca<-rasterPCA(zone1_1var,spca=TRUE)
# compare results from the 2 PCAs
summary(pca$model)
summary(res.pca)
plot(pca$map)

# Maps for PCA
# using simple plot function:
pc<-stack(subset(pca$map,c(1:4))) # change 1:4 based on how many PCs you want to plot
#plot(pc) # this plots the "new variables" that are the combination of the original variables making up the PCs
#writeRaster(pca$map, "output_data/pca.tif")

# scaled maps:
pc_scale<-(pc-cellStats(pc,"min"))/(cellStats(pc,"max")-cellStats(pc,"min")) # scaling the raster stack that is the outputs of PCs 1-4
#pc_wgs<-projectRaster(pc_scale,crs="+init=epsg:4326") # change coordinate system to WGS84
#plot(pc_wgs)

# Using ggplot:
pc.p <- rasterToPoints(pc_scale) #Make the points a dataframe for ggplot
pc.df <- data.frame(pc.p)

#minlon<-min(pc.df$x-0.2)
#maxlon<-max(pc.df$x+0.2)
#minlat<-min(pc.df$y-0.2)
#maxlat<-max(pc.df$y+0.2)

#d<-melt(pc.df,id=c('x','y')) # used to convert a data frame with several measurement columns into a data frame in a canonical format, which has one row for every observed (measured) value (see R self-help notes). The ggplot function requires that your data are in a melted format

#mapWorld <- borders("world", colour="gray50", fill="gray50", alpha=.7) # create a layer of borders

#ggplot()+geom_raster(data=d,aes(y=y,x=x, fill=value))+theme_bw()+
# geom_path(data = isobath_df, 
#           aes(x = long, y = lat, group = group),
#           linetype = "dashed", size = .2)+
#  facet_wrap(~variable, ncol=2)+
#  coord_equal(xlim = c(minlon, maxlon),ylim = c(minlat, maxlat))+
#  xlab(NULL)+ylab(NULL)+
#  scale_fill_distiller(palette = "Spectral")+
#  theme(legend.position="none",
#        strip.text.x = element_text(size = 8),
#        axis.title=element_blank(),
#        axis.text=element_blank(),
#        axis.ticks=element_blank(),
#        line=element_blank())+
#  mapWorld

#ggsave('figures/Maps_Scaled PCs 1-4_ggplot.jpg', dpi=150, units='in', width=13, height=7.5)

## Correlation among descriptors and PCs
zone1_1var_pca<-stack(zone1_1var, pc) # raster stack of input variables and pca outputs for pcs 1-3

pca.df<-as.data.frame(zone1_1var_pca)
pca.df<-pca.df[complete.cases(pca.df),]
c<-cor(pca.df, method="pearson") # computes correlation between variables and pcs
write.table(c, file='./Test1/zone1.1_correlation.csv', dec = '.', sep=';') # if this command doesn't work, check have made a folder called "figures"

# Plot correlogram
#setwd("C:/Users/kmcquaid/OneDrive - University of Plymouth/2 Methods/Data & Projects/S Atlantic/Classification/Draft 3")
#png(height=500, width=700, file="corplot.png", type = "cairo")
#corrplot(c,method='square',type='lower') # this isn't currently working...not sure why
#dev.off()

######## CLASSIFICATION

## Calinski-Harabasz index:
df <- as.data.frame(scale(pc))
df<-as.data.frame(df[complete.cases(df),])
# loop to calculate CH index (CI) for a number of iterations (change 2:30 to different max iterations)
ci=data.frame(1,NA)
names(ci)=c('i','cii')
for(i in c(2:40)){
  c<-kmeans(df, i)
  cii<-calinhara(df,c$cluster,cn=i)
  ci<-rbind(ci,data.frame(i,cii))
}
ci%>%arrange(-cii) # Get highest values of CH Index

#Plot CI
#ggplot(ci,aes(x=i,y=cii/10000))+geom_point(size=.5)+geom_line(size=.3)+
#  theme_bw()+xlab('Clusters')+ylab('Calinski-Harabasz Index')+
#  geom_vline(xintercept = 5, linetype=2)+
#  geom_vline(xintercept = 4, linetype=2)+
#  geom_vline(xintercept = 6, linetype=2)
# this adds lines to where you see peaks in the graph, i.e. where the CH index is highest
#ggsave("figures/ch_index.jpg", 
#       width = 13 , height = 6, units = 'in', dpi = 200)

## Average Silhouette Width (ASW):
pc.pamk <- pc.df[,c(3:6)]
clara <- pamk(pc.pamk, krange=2:40,criterion="asw", usepam=FALSE,
              scaling=FALSE, diss=inherits(pc.pamk, "Euclidean"),
              critout=TRUE)
## add line of code to arrange according to highest values of ASW

######## Carry out cluster analysis

clara <- pamk(pc.pamk, krange=6,criterion="asw", usepam=FALSE,
              scaling=FALSE, diss=inherits(pc.pamk, "Euclidean"),
              critout=TRUE) # change 4 above to whatever number of clusters you want

clara.out <- data.frame(pc.df$x, pc.df$y, clara$pamobject$clustering) # output with x, y, and cluster solution
clara.rast <- rasterFromXYZ(clara.out) # convert data frame to raster
crs(clara.rast) <- "+init=epsg:4326" # change coordinate system to WGS84
plot(clara.rast)
writeRaster(clara.rast, "./Test1/Level3_zone1-1_6classes.tif", overwrite = TRUE)

### This is the final output for the most detailed level of clustering

######## DECISION TREE

names(clara.rast)<-'zone' # rename clustering output to something sensible
zones <- clara.rast
s<-stack(zone1_1var,zones)

s.df<-as.data.frame(s)

s.df<-s.df[complete.cases(s.df),]
s.df$zone<-factor(s.df$zone)

bfit <- rpart(zone~., method="class", data=s.df) # rpart used for partitioning and regression trees
#printcp(bfit) # display the results 
#plotcp(bfit) # visualize cross-validation results 
#summary(bfit) # detailed summary of splits

jpeg("./Test1/Level2-zone1_1_decision_tree.jpg", width=13, height = 7, units = 'in', res = 300)
rpart.plot(bfit, type=0, nn=FALSE, family = 'Arial',box.palette = 'Blues')
dev.off()

###########     RE-CLUSTERING OF HABITAT CLASS 1 SUBCLASS 2      #################################################

######## PRINCIPLE COMPONENT ANALYSIS

## PCA using "prcomp"
gc()
zone1_2var.df <- as.data.frame(zone1_2var)
zone1_2var.df <- zone1_2var.df[complete.cases(zone1_2var.df),]
head(zone1_2var.df)
res.pca<-prcomp(zone1_2var.df,scale = TRUE)
summary(res.pca)
plot(res.pca)
# PCA Wheel (loading plot):
# plotting the first two PCs (called dimensions in the plot)
#fviz_pca_var(res.pca, axes = c(1,2),
#             col.var = "contrib", # Color by contributions to the PC
#             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
#             repel = TRUE,     # Avoid text overlapping
#             title='')
#ggsave("figures/pca_wheel_1-2.jpg", dpi=150, units='in', width=7, height=7)

# plotting the second two PCs
#fviz_pca_var(res.pca, axes = c(3,4),
#             col.var = "contrib", # Color by contributions to the PC
#             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
#             repel = TRUE,     # Avoid text overlapping
#             title='')
#ggsave("figures/pca_wheel_3-4.jpg", dpi=150, units='in', width=7, height=7)

## PCA using "raster"
pca<-rasterPCA(zone1_2var,spca=TRUE)
# compare results from the 2 PCAs
summary(pca$model)
summary(res.pca)
#plot(pca$map)

# Maps for PCA
# using simple plot function:
pc<-stack(subset(pca$map,c(1:4))) # change 1:4 based on how many PCs you want to plot
#plot(pc) # this plots the "new variables" that are the combination of the original variables making up the PCs
#writeRaster(pca$map, "output_data/pca.tif")

# scaled maps:
pc_scale<-(pc-cellStats(pc,"min"))/(cellStats(pc,"max")-cellStats(pc,"min")) # scaling the raster stack that is the outputs of PCs 1-4
#pc_wgs<-projectRaster(pc_scale,crs="+init=epsg:4326") # change coordinate system to WGS84
#plot(pc_wgs)

# Using ggplot:
pc.p <- rasterToPoints(pc_scale) #Make the points a dataframe for ggplot
pc.df <- data.frame(pc.p)

#minlon<-min(pc.df$x-0.2)
#maxlon<-max(pc.df$x+0.2)
#minlat<-min(pc.df$y-0.2)
#maxlat<-max(pc.df$y+0.2)

#d<-melt(pc.df,id=c('x','y')) # used to convert a data frame with several measurement columns into a data frame in a canonical format, which has one row for every observed (measured) value (see R self-help notes). The ggplot function requires that your data are in a melted format

#mapWorld <- borders("world", colour="gray50", fill="gray50", alpha=.7) # create a layer of borders

#ggplot()+geom_raster(data=d,aes(y=y,x=x, fill=value))+theme_bw()+
# geom_path(data = isobath_df, 
#           aes(x = long, y = lat, group = group),
#           linetype = "dashed", size = .2)+
#  facet_wrap(~variable, ncol=2)+
#  coord_equal(xlim = c(minlon, maxlon),ylim = c(minlat, maxlat))+
#  xlab(NULL)+ylab(NULL)+
#  scale_fill_distiller(palette = "Spectral")+
#  theme(legend.position="none",
#        strip.text.x = element_text(size = 8),
#        axis.title=element_blank(),
#        axis.text=element_blank(),
#        axis.ticks=element_blank(),
#        line=element_blank())+
#  mapWorld

#ggsave('figures/Maps_Scaled PCs 1-4_ggplot.jpg', dpi=150, units='in', width=13, height=7.5)

## Correlation among descriptors and PCs
zone1_2var_pca<-stack(zone1_2var, pc) # raster stack of input variables and pca outputs for pcs 1-3

pca.df<-as.data.frame(zone1_2var_pca)
pca.df<-pca.df[complete.cases(pca.df),]
c<-cor(pca.df, method="pearson") # computes correlation between variables and pcs
write.table(c, file='./Test1/zone1-2_correlation.csv', dec = '.', sep=';') # if this command doesn't work, check have made a folder called "figures"

# Plot correlogram
#setwd("C:/Users/kmcquaid/OneDrive - University of Plymouth/2 Methods/Data & Projects/S Atlantic/Classification/Draft 3")
#png(height=500, width=700, file="corplot.png", type = "cairo")
#corrplot(c,method='square',type='lower') # this isn't currently working...not sure why
#dev.off()

######## CLASSIFICATION

## Calinski-Harabasz index:
df <- as.data.frame(scale(pc))
df<-as.data.frame(df[complete.cases(df),])
# loop to calculate CH index (CI) for a number of iterations (change 2:30 to different max iterations)
ci=data.frame(1,NA)
names(ci)=c('i','cii')
for(i in c(2:40)){
  c<-kmeans(df, i)
  cii<-calinhara(df,c$cluster,cn=i)
  ci<-rbind(ci,data.frame(i,cii))
}
ci%>%arrange(-cii) # Get highest values of CH Index

#Plot CI
#ggplot(ci,aes(x=i,y=cii/10000))+geom_point(size=.5)+geom_line(size=.3)+
#  theme_bw()+xlab('Clusters')+ylab('Calinski-Harabasz Index')+
#  geom_vline(xintercept = 5, linetype=2)+
#  geom_vline(xintercept = 4, linetype=2)+
#  geom_vline(xintercept = 6, linetype=2)
# this adds lines to where you see peaks in the graph, i.e. where the CH index is highest
#ggsave("figures/ch_index.jpg", 
#       width = 13 , height = 6, units = 'in', dpi = 200)

## Average Silhouette Width (ASW):
pc.pamk <- pc.df[,c(3:6)]
clara <- pamk(pc.pamk, krange=2:40,criterion="asw", usepam=FALSE,
              scaling=FALSE, diss=inherits(pc.pamk, "Euclidean"),
              critout=TRUE)
## add line of code to arrange according to highest values of ASW

######## Carry out cluster analysis

clara <- pamk(pc.pamk, krange=11,criterion="asw", usepam=FALSE,
              scaling=FALSE, diss=inherits(pc.pamk, "Euclidean"),
              critout=TRUE) # change 4 above to whatever number of clusters you want

clara.out <- data.frame(pc.df$x, pc.df$y, clara$pamobject$clustering) # output with x, y, and cluster solution
clara.rast <- rasterFromXYZ(clara.out) # convert data frame to raster
crs(clara.rast) <- "+init=epsg:4326" # change coordinate system to WGS84
plot(clara.rast)
writeRaster(clara.rast, "./Test1/Level3_zone1-2_11classes.tif", overwrite = TRUE)

######## DECISION TREE

names(clara.rast)<-'zone' # rename clustering output to something sensible
zones <- clara.rast

## the output of clara clustering is a different extent to the input zone 2 vars, I'm not sure why...So have to crop to same extent
zone1_2var.crop <- crop(zone1_2var, zones)
s<-stack(zone1_2var.crop, zones)

s.df<-as.data.frame(s)

s.df<-s.df[complete.cases(s.df),]
s.df$zone<-factor(s.df$zone)

bfit <- rpart(zone~., method="class", data=s.df) # rpart used for partitioning and regression trees
#printcp(bfit) # display the results 
#plotcp(bfit) # visualize cross-validation results 
#summary(bfit) # detailed summary of splits

jpeg("./Test1/Level2-zone1_2_decision_tree.jpg", width=13, height = 7, units = 'in', res = 300)
rpart.plot(bfit, type=0, nn=FALSE, family = 'Arial',box.palette = 'Blues')
dev.off()
###########     RE-CLUSTERING OF HABITAT CLASS 1 SUBCLASS 3      #################################################

######## PRINCIPLE COMPONENT ANALYSIS

## PCA using "prcomp"
gc()
zone1_3var.df <- as.data.frame(zone1_3var)
zone1_3var.df <- zone1_3var.df[complete.cases(zone1_3var.df),]
head(zone1_3var.df)
res.pca<-prcomp(zone1_3var.df,scale = TRUE)
summary(res.pca)
#plot(res.pca)
# PCA Wheel (loading plot):
# plotting the first two PCs (called dimensions in the plot)
#fviz_pca_var(res.pca, axes = c(1,2),
#             col.var = "contrib", # Color by contributions to the PC
#             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
#             repel = TRUE,     # Avoid text overlapping
#             title='')
#ggsave("figures/pca_wheel_1-2.jpg", dpi=150, units='in', width=7, height=7)

# plotting the second two PCs
#fviz_pca_var(res.pca, axes = c(3,4),
#             col.var = "contrib", # Color by contributions to the PC
#             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
#             repel = TRUE,     # Avoid text overlapping
#             title='')
#ggsave("figures/pca_wheel_3-4.jpg", dpi=150, units='in', width=7, height=7)

## PCA using "raster"
pca<-rasterPCA(zone1_3var,spca=TRUE)
# compare results from the 2 PCAs
summary(pca$model)
summary(res.pca)
plot(pca$map)

# Maps for PCA
# using simple plot function:
pc<-stack(subset(pca$map,c(1:4))) # change 1:4 based on how many PCs you want to plot
#plot(pc) # this plots the "new variables" that are the combination of the original variables making up the PCs
#writeRaster(pca$map, "output_data/pca.tif")

# scaled maps:
pc_scale<-(pc-cellStats(pc,"min"))/(cellStats(pc,"max")-cellStats(pc,"min")) # scaling the raster stack that is the outputs of PCs 1-4
#pc_wgs<-projectRaster(pc_scale,crs="+init=epsg:4326") # change coordinate system to WGS84
#plot(pc_wgs)

# Using ggplot:
pc.p <- rasterToPoints(pc_scale) #Make the points a dataframe for ggplot
pc.df <- data.frame(pc.p)

#minlon<-min(pc.df$x-0.2)
#maxlon<-max(pc.df$x+0.2)
#minlat<-min(pc.df$y-0.2)
#maxlat<-max(pc.df$y+0.2)

#d<-melt(pc.df,id=c('x','y')) # used to convert a data frame with several measurement columns into a data frame in a canonical format, which has one row for every observed (measured) value (see R self-help notes). The ggplot function requires that your data are in a melted format

#mapWorld <- borders("world", colour="gray50", fill="gray50", alpha=.7) # create a layer of borders

#ggplot()+geom_raster(data=d,aes(y=y,x=x, fill=value))+theme_bw()+
# geom_path(data = isobath_df, 
#           aes(x = long, y = lat, group = group),
#           linetype = "dashed", size = .2)+
#  facet_wrap(~variable, ncol=2)+
#  coord_equal(xlim = c(minlon, maxlon),ylim = c(minlat, maxlat))+
#  xlab(NULL)+ylab(NULL)+
#  scale_fill_distiller(palette = "Spectral")+
#  theme(legend.position="none",
#        strip.text.x = element_text(size = 8),
#        axis.title=element_blank(),
#        axis.text=element_blank(),
#        axis.ticks=element_blank(),
#        line=element_blank())+
#  mapWorld

#ggsave('figures/Maps_Scaled PCs 1-4_ggplot.jpg', dpi=150, units='in', width=13, height=7.5)

## Correlation among descriptors and PCs
zone1_3var_pca<-stack(zone1_3var, pc) # raster stack of input variables and pca outputs for pcs 1-4

pca.df<-as.data.frame(zone1_3var_pca)
pca.df<-pca.df[complete.cases(pca.df),]
c<-cor(pca.df, method="pearson") # computes correlation between variables and pcs
write.table(c, file='./Test1/zone1-3_correlation.csv', dec = '.', sep=';') # if this command doesn't work, check have made a folder called "figures"

# Plot correlogram
#setwd("C:/Users/kmcquaid/OneDrive - University of Plymouth/2 Methods/Data & Projects/S Atlantic/Classification/Draft 3")
#png(height=500, width=700, file="corplot.png", type = "cairo")
#corrplot(c,method='square',type='lower') # this isn't currently working...not sure why
#dev.off()

######## CLASSIFICATION

## Calinski-Harabasz index:
df <- as.data.frame(scale(pc))
df<-as.data.frame(df[complete.cases(df),])
# loop to calculate CH index (CI) for a number of iterations (change 2:30 to different max iterations)
ci=data.frame(1,NA)
names(ci)=c('i','cii')
for(i in c(2:40)){
  c<-kmeans(df, i)
  cii<-calinhara(df,c$cluster,cn=i)
  ci<-rbind(ci,data.frame(i,cii))
}
ci%>%arrange(-cii) # Get highest values of CH Index

#Plot CI
#ggplot(ci,aes(x=i,y=cii/10000))+geom_point(size=.5)+geom_line(size=.3)+
#  theme_bw()+xlab('Clusters')+ylab('Calinski-Harabasz Index')+
#  geom_vline(xintercept = 5, linetype=2)+
#  geom_vline(xintercept = 4, linetype=2)+
#  geom_vline(xintercept = 6, linetype=2)
# this adds lines to where you see peaks in the graph, i.e. where the CH index is highest
#ggsave("figures/ch_index.jpg", 
#       width = 13 , height = 6, units = 'in', dpi = 200)

## Average Silhouette Width (ASW):
pc.pamk <- pc.df[,c(3:6)]
clara <- pamk(pc.pamk, krange=2:40,criterion="asw", usepam=FALSE,
              scaling=FALSE, diss=inherits(pc.pamk, "Euclidean"),
              critout=TRUE)
## add line of code to arrange according to highest values of ASW

######## Carry out cluster analysis

clara <- pamk(pc.pamk, krange=5,criterion="asw", usepam=FALSE,
              scaling=FALSE, diss=inherits(pc.pamk, "Euclidean"),
              critout=TRUE) # change 4 above to whatever number of clusters you want

clara.out <- data.frame(pc.df$x, pc.df$y, clara$pamobject$clustering) # output with x, y, and cluster solution
clara.rast <- rasterFromXYZ(clara.out) # convert data frame to raster
crs(clara.rast) <- "+init=epsg:4326" # change coordinate system to WGS84
plot(clara.rast)
writeRaster(clara.rast, "./Test1/Level3_zone1-3_5classes.tif", overwrite = TRUE)

######## DECISION TREE

names(clara.rast)<-'zone' # rename clustering output to something sensible
zones <- clara.rast

## the output of clara clustering is a different extent to the input zone 2 vars, I'm not sure why...So have to crop to same extent
zone1_3var.crop <- crop(zone1_3var, zones)
s<-stack(zone1_3var.crop, zones)

s.df<-as.data.frame(s)

s.df<-s.df[complete.cases(s.df),]
s.df$zone<-factor(s.df$zone)

bfit <- rpart(zone~., method="class", data=s.df) # rpart used for partitioning and regression trees
#printcp(bfit) # display the results 
#plotcp(bfit) # visualize cross-validation results 
#summary(bfit) # detailed summary of splits

jpeg("./Test1/Level2-zone1_3_decision_tree.jpg", width=13, height = 7, units = 'in', res = 300)
rpart.plot(bfit, type=0, nn=FALSE, family = 'Arial',box.palette = 'Blues')
dev.off()

###########     RE-CLUSTERING OF HABITAT CLASS 1 SUBCLASS 4      #################################################

######## PRINCIPLE COMPONENT ANALYSIS

## PCA using "prcomp"
gc()
zone1_4var.df <- as.data.frame(zone1_4var)
zone1_4var.df <- zone1_4var.df[complete.cases(zone1_4var.df),]
head(zone1_4var.df)
res.pca<-prcomp(zone1_4var.df,scale = TRUE)
summary(res.pca)
#plot(res.pca)
# PCA Wheel (loading plot):
# plotting the first two PCs (called dimensions in the plot)
#fviz_pca_var(res.pca, axes = c(1,2),
#             col.var = "contrib", # Color by contributions to the PC
#             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
#             repel = TRUE,     # Avoid text overlapping
#             title='')
#ggsave("figures/pca_wheel_1-2.jpg", dpi=150, units='in', width=7, height=7)

# plotting the second two PCs
#fviz_pca_var(res.pca, axes = c(3,4),
#             col.var = "contrib", # Color by contributions to the PC
#             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
#             repel = TRUE,     # Avoid text overlapping
#             title='')
#ggsave("figures/pca_wheel_3-4.jpg", dpi=150, units='in', width=7, height=7)

## PCA using "raster"
pca<-rasterPCA(zone1_4var,spca=TRUE)
# compare results from the 2 PCAs
summary(pca$model)
summary(res.pca)
#plot(pca$map)

# Maps for PCA
# using simple plot function:
pc<-stack(subset(pca$map,c(1:4))) # change 1:4 based on how many PCs you want to plot
#plot(pc) # this plots the "new variables" that are the combination of the original variables making up the PCs
#writeRaster(pca$map, "output_data/pca.tif")

# scaled maps:
pc_scale<-(pc-cellStats(pc,"min"))/(cellStats(pc,"max")-cellStats(pc,"min")) # scaling the raster stack that is the outputs of PCs 1-4
#pc_wgs<-projectRaster(pc_scale,crs="+init=epsg:4326") # change coordinate system to WGS84
#plot(pc_wgs)

# Using ggplot:
pc.p <- rasterToPoints(pc_scale) #Make the points a dataframe for ggplot
pc.df <- data.frame(pc.p)

#minlon<-min(pc.df$x-0.2)
#maxlon<-max(pc.df$x+0.2)
#minlat<-min(pc.df$y-0.2)
#maxlat<-max(pc.df$y+0.2)

#d<-melt(pc.df,id=c('x','y')) # used to convert a data frame with several measurement columns into a data frame in a canonical format, which has one row for every observed (measured) value (see R self-help notes). The ggplot function requires that your data are in a melted format

#mapWorld <- borders("world", colour="gray50", fill="gray50", alpha=.7) # create a layer of borders

#ggplot()+geom_raster(data=d,aes(y=y,x=x, fill=value))+theme_bw()+
# geom_path(data = isobath_df, 
#           aes(x = long, y = lat, group = group),
#           linetype = "dashed", size = .2)+
#  facet_wrap(~variable, ncol=2)+
#  coord_equal(xlim = c(minlon, maxlon),ylim = c(minlat, maxlat))+
#  xlab(NULL)+ylab(NULL)+
#  scale_fill_distiller(palette = "Spectral")+
#  theme(legend.position="none",
#        strip.text.x = element_text(size = 8),
#        axis.title=element_blank(),
#        axis.text=element_blank(),
#        axis.ticks=element_blank(),
#        line=element_blank())+
#  mapWorld

#ggsave('figures/Maps_Scaled PCs 1-4_ggplot.jpg', dpi=150, units='in', width=13, height=7.5)

## Correlation among descriptors and PCs
zone1_4var_pca<-stack(zone1_4var, pc) # raster stack of input variables and pca outputs for pcs 1-3

pca.df<-as.data.frame(zone1_4var_pca)
pca.df<-pca.df[complete.cases(pca.df),]
c<-cor(pca.df, method="pearson") # computes correlation between variables and pcs
write.table(c, file='./Test1/zone1-4_correlation.csv', dec = '.', sep=';') # if this command doesn't work, check have made a folder called "figures"

# Plot correlogram
#setwd("C:/Users/kmcquaid/OneDrive - University of Plymouth/2 Methods/Data & Projects/S Atlantic/Classification/Draft 3")
#png(height=500, width=700, file="corplot.png", type = "cairo")
#corrplot(c,method='square',type='lower') # this isn't currently working...not sure why
#dev.off()

######## CLASSIFICATION

## Calinski-Harabasz index:
df <- as.data.frame(scale(pc))
df<-as.data.frame(df[complete.cases(df),])
# loop to calculate CH index (CI) for a number of iterations (change 2:30 to different max iterations)
ci=data.frame(1,NA)
names(ci)=c('i','cii')
for(i in c(2:40)){
  c<-kmeans(df, i)
  cii<-calinhara(df,c$cluster,cn=i)
  ci<-rbind(ci,data.frame(i,cii))
}
ci%>%arrange(-cii) # Get highest values of CH Index

#Plot CI
#ggplot(ci,aes(x=i,y=cii/10000))+geom_point(size=.5)+geom_line(size=.3)+
#  theme_bw()+xlab('Clusters')+ylab('Calinski-Harabasz Index')+
#  geom_vline(xintercept = 5, linetype=2)+
#  geom_vline(xintercept = 4, linetype=2)+
#  geom_vline(xintercept = 6, linetype=2)
# this adds lines to where you see peaks in the graph, i.e. where the CH index is highest
#ggsave("figures/ch_index.jpg", 
#       width = 13 , height = 6, units = 'in', dpi = 200)

## Average Silhouette Width (ASW):
pc.pamk <- pc.df[,c(3:6)]
clara <- pamk(pc.pamk, krange=2:40,criterion="asw", usepam=FALSE,
              scaling=FALSE, diss=inherits(pc.pamk, "Euclidean"),
              critout=TRUE)
## add line of code to arrange according to highest values of ASW

######## Carry out cluster analysis

clara <- pamk(pc.pamk, krange=7,criterion="asw", usepam=FALSE,
              scaling=FALSE, diss=inherits(pc.pamk, "Euclidean"),
              critout=TRUE) # change 4 above to whatever number of clusters you want

clara.out <- data.frame(pc.df$x, pc.df$y, clara$pamobject$clustering) # output with x, y, and cluster solution
clara.rast <- rasterFromXYZ(clara.out) # convert data frame to raster
crs(clara.rast) <- "+init=epsg:4326" # change coordinate system to WGS84
plot(clara.rast)
writeRaster(clara.rast, "./Test1/Level3_zone1-4_7classes.tif", overwrite = TRUE)

######## DECISION TREE

names(clara.rast)<-'zone' # rename clustering output to something sensible
zones <- clara.rast

## the output of clara clustering is a different extent to the input zone 2 vars, I'm not sure why...So have to crop to same extent
zone1_4var.crop <- crop(zone1_4var, zones)
s<-stack(zone1_4var.crop, zones)

s.df<-as.data.frame(s)

s.df<-s.df[complete.cases(s.df),]
s.df$zone<-factor(s.df$zone)

bfit <- rpart(zone~., method="class", data=s.df) # rpart used for partitioning and regression trees
#printcp(bfit) # display the results 
#plotcp(bfit) # visualize cross-validation results 
#summary(bfit) # detailed summary of splits

jpeg("./Test1/Level2-zone1_4_decision_tree.jpg", width=13, height = 7, units = 'in', res = 300)
rpart.plot(bfit, type=0, nn=FALSE, family = 'Arial',box.palette = 'Blues')
dev.off()