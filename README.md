# Atlantic Ecoregions

## Description

This project contained some work related to McQuaid et al. (2023), where we were applying environmental classifications to Bio-ORACLE (mostly) datasets. As part of the broader collaboration around that paper, we also applied the 'hierarchical classification' approach (a PCA analysis followed by kmeans clustering of the principal components; see paper and the scripts in this project) to a regional subset of the data around South Africa.

The original scripts were provided by [Tiago Gandra](https://scholar.google.com/citations?user=8UCQfukAAAAJ&hl=en&oi=sra). I'd like to link to his GitHub account, but was having trouble finding it at the time of writing this.

## References

McQuaid, K. A., Bridges, A. E. H., Howell, K. L., Gandra, T. B. R., de Souza, V., Currie, J. C., et al. (2023). Broad-scale benthic habitat classification of the South Atlantic. Progress in Oceanography 214, 103016. doi: 10.1016/j.pocean.2023.103016.
